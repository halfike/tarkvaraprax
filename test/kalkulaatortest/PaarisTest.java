package kalkulaatortest;

import static org.junit.Assert.*;
import kalkulaator.Paaris;

import org.junit.Test;

public class PaarisTest {
	@Test
	public void test()
	{
		assertEquals(2, Paaris.paaris(new int[] {2} ));
		assertEquals(12, Paaris.paaris(new int[] {2, 4, 6} ));
		assertEquals(12, Paaris.paaris(new int[] {-2, -4, 2, 4, 6}));
		assertEquals(12, Paaris.paaris(new int[] {1, 2, 4, 6}));
		assertEquals(12, Paaris.paaris(new int[] {-2, -4, 1, 2, 4, 6}));
	}
}
